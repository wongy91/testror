class ArticlesController < ApplicationController
  def find_article
    article_id = params[:id]

    @article = Article.find(article_id)

    render json: {
      owner_name: @article.owner.name,
      article: @article
    }
  end
end
