class OwnersController < ApplicationController
  def all_owners
    @owners = Owner.all
    render json: { owners: @owners }
  end

  def all_articles
    name = params[:owner_name]
    @owner = Owner.find_by(name: name)

    render json: {
      owner_name: @owner.name,
      articles: @owner.articles
    }
  end

  def find_owner
    name = params[:owner_name]
    @owner = Owner.find_by(name: name)

    render json: {
      owner_name: @owner.name
    }
  end
end