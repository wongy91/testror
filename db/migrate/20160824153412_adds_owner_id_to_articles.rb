class AddsOwnerIdToArticles < ActiveRecord::Migration
  def change
    add_column :articles, :owner_id, :int
  end
end
