# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

owner_01 = Owner.create(name: "Jack")
owner_02 = Owner.create(name: "Jonas")
owner_03 = Owner.create(name: "Neil")

Article.create(title: "I Love California", owner_id: owner_01.id)
Article.create(title: "I Love West Lafayette", owner_id: owner_01.id)
Article.create(title: "I Love Chicago", owner_id: owner_01.id)

Article.create(title: "I Love All", owner_id: owner_02.id)

